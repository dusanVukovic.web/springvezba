$(document).ready(function() {
  var id = location.search.split('id=')[1];
  hej(id);
});

function hej(id){

  var table = $('#table');
  var newRow = "";
  $.get("http://localhost:9000/api/students/"+id, function(data, status) {

      newRow =
      "<tr>"+
        "<th class=tb-h>First Name: </th>"+
        "<td class=tb-d>"+data.firstName+"</td>"+
      "</tr>"+
      "<tr>"+
        "<th class=tb-h>Last Name: </th>"+
        "<td class=tb-d>"+data.lastName+"</td>"+
      "</tr>"+
      "<tr>"+
        "<th class=tb-h>Email: </th>"+
        "<td class=tb-d>"+data.email+"</td>"+
      "</tr>";

      if(data.image === null){
        $('#profileImg').attr("src","img/avatar.png");
      }else{
        $('#profileImg').attr("src","img/"+data.image);
      }

      table.append(newRow);


  });

}
