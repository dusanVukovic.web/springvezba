package com.vezba.controller;

import com.vezba.entity.Student;
import com.vezba.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*")
public class StudentContorller {

    private StudentService studentService;

    @Autowired
    public StudentContorller(StudentService theStudentService){
        this.studentService = theStudentService;
    }


    @GetMapping("/students")
    public List<Student> getAll(){
        return studentService.findAll();
    }

    @GetMapping("students/{student_id}")
    public Student getStudent(@PathVariable ("student_id") int studentId){
        System.out.println("Hello")
        return studentService.findStudentById(studentId);
    }

    @PostMapping("students")
    public void saveStudent(@RequestBody Student theStudent){
        studentService.saveStudent(theStudent);
    }

    @DeleteMapping("students/{student_id}")
    public void DeleteStudent(@PathVariable("student_id") int id){
        studentService.deleteStudent(id);
    }
}
