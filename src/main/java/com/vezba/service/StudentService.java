package com.vezba.service;

import com.vezba.entity.Student;

import java.util.List;

public interface StudentService {
    public List<Student> findAll();
    public Student findStudentById(int id);
    public void deleteStudent(int id);
    public void saveStudent(Student theStudent);
}
